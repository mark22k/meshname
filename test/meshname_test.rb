require 'minitest/autorun'

class TestMeshmame < Minitest::Test
  require 'ipaddr'
  require_relative '../lib/meshname'

  def test_ip_to_meshname
    assert_equal 'aikqcxee4cg5k5mqx7gwdt6p64', Meshname.getname(IPAddr.new('215:15c:84e0:8dd5:7590:bfcd:61cf:cff7'))
    assert_equal 'aikqcxee4cg5k5mqx7gwdt6p64.meship', "#{Meshname.getname IPAddr.new('215:15c:84e0:8dd5:7590:bfcd:61cf:cff7')}.meship"
  end

  def test_meshname_to_ip
    assert_equal IPAddr.new('215:15c:84e0:8dd5:7590:bfcd:61cf:cff7'), Meshname.getip('aikqcxee4cg5k5mqx7gwdt6p64')
    assert_equal '215:15c:84e0:8dd5:7590:bfcd:61cf:cff7', Meshname.getip('aikqcxee4cg5k5mqx7gwdt6p64').to_s
  end

  def test_meship_resolv
    assert_equal [IPAddr.new('215:15c:84e0:8dd5:7590:bfcd:61cf:cff7')], Meshname.resolv('aikqcxee4cg5k5mqx7gwdt6p64.meship')
  end
end
