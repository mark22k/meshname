Gem::Specification.new do |spec|
  spec.name        = 'meshname'
  spec.version     = '1.1.0'
  spec.summary     = 'Gem, which provides conversion and DNS resolution functions for the Meshname protocol.'
  spec.description = 'Gem, which provides conversion and DNS resolution functions for the Meshname protocol (see https://github.com/zhoreeq/meshname)'
  spec.authors     = ['Marek Küthe']
  spec.email       = 'm.k@mk16.de'

  spec.files       = %w[lib/meshname.rb]
  spec.extra_rdoc_files = %w[LICENSE README.md]

  spec.homepage    = 'https://codeberg.org/mark22k/meshname'
  spec.license     = 'GPL-3.0-or-later'

  spec.metadata = { 'source_code_uri' => 'https://codeberg.org/mark22k/meshname',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/meshname/issues',
                    'rubygems_mfa_required' => 'true' }

  spec.required_ruby_version = '>= 3.1'
  spec.add_dependency 'base32', '>= 0.3.4'
end
